<?php
	$p = isset($_GET['p']) ? $_GET['p'] : FALSE;
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="j/lgn.js"></script>	

<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55080941-1', 'auto');
	  ga('send', 'pageview');

</script>

<link type="text/css" rel="stylesheet" href="mode.css" />
<link href="http://fonts.googleapis.com/css?family=Signika:400,600" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Abel:400,600" rel="stylesheet" type="text/css">




<?php
function luci($l) {

	print('
			<!DOCTYPE html>
			<html>
			<head>
				<script type="text/javascript" src="j/luci.js"></script>
				<script type="text/javascript" src="j/jsn.js"></script>
				<script type="text/javascript" src="j/lgn.js"></script>
				
				<title>((ECHO))</title>
			</head>

			<body>
			<div id="scp">
				<div class="strscp one"></div>
				<div class="strscp two"></div>
				<div class="strscp three"></div>
			</div>

			 	<div class="lgn main">
					<h2>((Echo))</h2>	
					<h3>Inspired by <a target="_blank" href="http://1beat.org/" >OneBeat</a></h3>
					<input class="pw" type="password" placeholder="pass">
					<div id="log">login</div>

				</div>

			 	</div>
			 <div id="ecu">
			 	<div class="hd">
			 		<div class="wrp">
			 			<div id="tac"class="x">about</div>
						<div class="lo">logout</div>
						<div id="sts">stop</div>
					</div>
				</div>

				<div id="pop">
					<div id="ovr"></div>
				 	<div id="crc"></div>
				 	<div id="conf">You made an echo!</div>
				 	<div id="err">Oh No! You forgot something!</div>
				 	<div id="ner">Oh No! Please rename your file</div>
				 	<div id="upc">Your echo has been created!</div>
				 	<div id="upl">Your echo will appear soon.</div>
				</div>


				<div id="clstr" class="cf"></div>
				<div id="gxy"></div>
				<div id="abt">
					<div class="slides">
						<div class="nav">
							<ul>
								<li name="about" class="a">About</li>
								<li name="fellows">Fellows</li>
								<li name="dates">Dates</li>
								<li name="team">Team</li>
								<li name="contact">Contact</li>
								
							</ul>
						</div>
						<div class="slide a about one" name="about">
							<div class="info one">
								<h3>About ((Echo))</h3>
								<p><strong>Echo</strong> is an online space that amplifies and extends the international music residency and tour, <a href="http://1beat.org">OneBeat</a>, through web-based exchange, dialogue, and collaboration. Echo connects innovative musicians, artists, writers, and thinkers as Echo Fellows in a month-long interactive exchange, during which time Fellows engage in an online laboratory of artistic and scholarly ideas and experimentation. This interactive exchange culminates in a 30-day online exhibition that showcases these new works, and encourages exchange with the wider public. Echo proceeds in three stages:</p>
								<p><strong>I. Sharing: In the first stage of Echo (September 1, 2014)</strong>, Fellows are asked to offer existing creative or scholarly work that they would like to share with the Echo community, reflecting the mission of <a href="http://1beat.org">OneBeat</a>: "to celebrate the transformative power of the arts through the creation of original, inventive music; to develop ways that music can make a positive impact on our local and global communities; and to foster mutual understanding and cooperation across geographic and cultural barriers."</p>
								<p><strong>II. Collaborating: In the second stage of Echo (September 15-October 15, 2014)</strong>, Echo Fellows are connected to a small group of other Echo Fellows in "Ensembles," and asked to collaborate and/or respond to one another\'s work through creative or scholarly work. Collaborative online tools such as: <a href="http://www.kompoz.com/">Kompoz</a>, <a href="http://google.com/drive">Google Drive</a>, <a href="http://facebook.com/">Facebook</a>, among others, are utilized individually or collectively to keep Echo Fellows connected and collaborating.</p>
								<p><strong>III. Exhibiting: In the third and last stage of Echo (November 15th-December 15th, 2014)</strong>, the collective creative and scholarly works of all Echo Fellows are posted online on the Echo website for the global public to view, share, and interact with the various work of the Echo Fellows.</p>
							</div>
							<div class="info about two">
								<h3>About OneBeat</h3>
								<p><a href="http://1beat.org">OneBeat</a> is an international cultural exchange that celebrates the transformative power of the arts through the creation of original, inventive music, and people-to-people diplomacy. OneBeat brings young musicians (ages 19-35) from around the world to the U.S. for one month to collaboratively write, produce, and perform original music, and develop ways that music can make a positive impact on our local and global communities. More than a performance program, OneBeat balances three principles -- dialogue, creation, and social engagement -- to foster mutual understanding and cooperation among citizens of the world.</p>
							</div>
						</div>
						<div class="slide fellows cf three" name="fellows">
							<div class="info one">
								<h3>Echo Fellows</h3>
								<div class="ensemble one">
									<p><strong>Ensemble 1</strong></p>
									<p>Natasha Humera Ejaz -  Producer, Vocalist from Pakistan</p>
									<p>Roshy Kheshti - Academic from USA</p>
									<p>Svetlana Maras - Composer from Serbia</p>
									<p>Steven Baker - Academic from USA </p>
								</div>
								<div class="ensemble two">
									<p><strong>Ensemble 2</strong></p>
									<p>PPS - MC from Senegal</p>
									<p>Sasha Grbich - Academic from Australia</p>
									<p>Fuerza Mexica - Emcee from USA</p>
									<p>Dmitri Burmistrov - Beatboxer, Producer from Russia</p>
								</div>
								<div class="ensemble three">
									<p><strong>Ensemble 3</strong></p>
									<p>Malabika Brahma  - Vocalist from India</p>								
									<p>Giorgos Stylianou - Lute player, Sound Artist from Cyprus </p>
									<p>Shannon Werle  -  Academic from USA </p>
								</div>
								<div class="ensemble four">
									<p><strong>Ensemble 4</strong></p>
									<p>Lama Zakharia &amp; Jana Zeinedinne  -  Vocalists from Jordan</p>
									<p>Helio Vanimal - Singer, Instrumentalist from Mozambique</p>
									<p>Jay Hammond - Banjo Player, Academic from USA</p>
									<p>Helen Morgan - Artist, Sounder from United Kingdom</p>
								</div>
								<div class="ensemble five">
									<p><strong>Ensemble 5</strong></p>
									<p>Wei Wei - Composer from China </p>
									<p>Elen Fl&uuml;gge - Writer, Sound Artist from Germany/USA </p>
									<p>Weronika Partyka -  Singer, Clarinet Player from Poland </p>
									<p>Aditi Bhagwat -  Percussionist from India </p>
								</div>
								<div class="ensemble six">
									<p><strong>Ensemble 6</strong></p>
									<p>Aimilia Karapostoli - Academic from Greece </p>
									<p>Ruth Danon - Vocalist, Producer from Israel </p>
									<p>Leah Barclay - Composer, Sound Artist from Australia </p>
									<p>Sarah Alden - Violinist, Singer from USA</p>
								</div>
								<div class="ensemble seven">
									<p><strong>Ensemble 7</strong></p>
									<p>Nicole Hodges Persley - Academic, Theater Artist from USA</p>
									<p>Ahmed Mabrok - MC from Egypt </p>
									<p>Ruben Coll -  Researcher, Sound Activist from Spain</p>
									<p>Wey Chong Chua - Ruan from Malaysia </p>
								</div>
								<div class="ensemble eight">
									<p><strong>Ensemble 8</strong></p>
									<p>Elyse Tabet - Producer, Sound Artist from Lebanon</p>
									<p>Grupal Crew Collective - Artists from Columbia, Italy, and Spain</p>
									<p>Sharrell Luckett - Academic, Performance Artist from USA </p>
									<p>Amine Metani - Electronic Music Producer from Tunisia</p>
								</div>
								<div class="ensemble nine">
									<p><strong>Ensemble 9</strong></p>
									<p>Darbuka Siva -  Drummer, Bassist from India</a></p>
									<p>Laura Plana Gracia - Academic from United Kingdom </p>
									<p>Jeb Middlebrook - Sound Archivist, Researcher from USA</p>
								</div>				
							</div>	
						</div>	
						<div class="slide dates four" name="dates">
							<div class="info one">
								<h3>Important Dates</h3>
								<p><span>September 1:</span> Echo Fellows Submissions Due (11:59PM GMT)</p>
								<p><span>October 15:</span> Echo Ensembles Content Due (11:59PM GMT)</p>
								<p><span>November 15 - December 15: </span> Public Exhibition</p>
							</div>
						</div>
						<div class="slide team five" name="team">
							<div class="info one">
								<h3>Team</h3>
								<div class="p">
									<p><a href="http://jebmiddlebrook.com">Jeb Middlebrook</a>, is Co-Founder and Curator of Echo. He is Assistant Professor of Sociology at California State University, Dominguez Hills in Los Angeles, where he researches and teaches music, justice, and ethnic studies. He holds a Ph.D. and M.A. in American Studies and Ethnicity from the University of Southern California, and a B.A. in Ethnic Studies from the University of Minnesota. His current work explores the sound of prison in popular culture, policy, and protest, and can be viewed at: <a href="http://prisonmusic.net">PrisonMusic.net</a>. </p>
								</div>
								<div class="p">
									<p><a href="">Chris Marianetti </a> , Co-Founder &amp; Artistic Director of Found Sound Nation, is a composer and producer from Albuquerque, New Mexico. His works have premiered at the International Gaudeamus Competition, Merkin Concert Hall, Massachusetts Museum of Modern Art, The Stone, and the Tenri Cultural Institute among others. Recent works include: an animated score for brass band projected onto the sides of Manhattan buildings, and a symphonic work for a group of lowrider automobiles for the 2012 ISEA festival in New Mexico. With Found Sound Nation Chris has led site-specific music projects in New York City, India, Mexico, Zimbabwe, Italy, and Switzerland and was recently awarded the PopTech Social Innovation Fellows Award. Chris is an active teaching artist with the Weill Music Institute at Carnegie Hall. Chris trained in composition at the Accademia Internazionale della Musica in Milan, Italy and received his Master\'s in music composition from Brooklyn College Conservatory of Music.</p>
								</div>
								<div class="p">
									<p><a href="">Elena Moon Park </a> is a musician and educator living in Brooklyn, NY. Originally from Oak Ridge, TN, she studied anthropology and ethnomusicology at Northwestern University and completed her M.A. in Urban Policy from The New School in New York. Before moving to the city, Elena served as a development consultant for several grassroots social justice organizations on the south and west sides of Chicago. Today, she manages programs for Bang on a Can\'s Found Sound Nation and actively engages in outreach with youth arts groups across the United States. Elena has recently released Rabbit Days and Dumplings, an album for families featuring folk and children\'s music from East Asia. Elena still finds the time to play fiddle, trumpet, mandolin, and sing with Dan Zanes and Friends on stages worldwide. </p>
								</div>
								<div class="p">
									<p><a href="">Jeremy Thal</a>, Co-Founder & Artistic Director of Found Sound Nation, is a horn player, composer, and educator. He studied horn, ethnomusicology, and Chinese at Northwestern University, and continues to work as a performer, playing in major music festivals and obscure bookstores around the world. With fellow co-founder Chris Marianetti, Jeremy co-designed Found Sound Nation\'s approach to socially-engaged music creation, and has co-led FSN projects in Haiti, Zimbabwe, New Orleans, Mexico, Indonesia, Italy, and Switzerland. For the last 3 years, Jeremy has served as a lead teaching artist at the Weill Music Institute at Carnegie Hall, running innovative music production workshops with incarcerated youth in New York City. A founder of ZeroBit Music, he composes and produces music for film, theater, web sites, and video games. As a horn player Jeremy has recorded and toured with indie rock heavyweights Jeff Mangum and The National, and leads his own band, Briars of North America. </p>
								</div>
								<div class="p">
									<p><a href="">Ezra Tenenbaum</a> is a New York-based performer and music producer. Ezra joined Found Sound Nation as a Teaching Artist in 2010 and has contributed to projects at Blair Grocery in New Orleans, Bronx\'s Horizons Juvenile Center, Brooklyn Prospect Charter School, and Brooklyn Community Arts and Media High School. He has performed at Glasslands, Death By Audio, Dance New Amsterdam and and the Center for Performance Research. In 2011, his band Strange Shapes released Star Chart on Japanese label, Rallye. </p>
								</div>
								<div class="p">
									<p><a href="">Miles Bridges</a>, Co-Creator of Lucid Structure, is an Oakland born Brooklyn based music producer and web artist. Lucid Structure engineers systems lucid in user experience and structured by data interaction. Recent projects include <a target="_blank" href="http://1beat.org">1beat.org</a> and Echo. Miles holds a BA in French Language and Literature from Boston University. Miles is a part of a small international music collective <a target="_blank" href="http://lucilala.co">lucilala</a> where he produces under the name Th&eacute;o Mode.</p>
								</div>
								<div class="p">
									<p><a href="">Shiran Sukumar</a> Co-Creator of Lucid Structure, is a web artist and engineer. Lucid Structure engineers systems lucid in user experience and structured by data interaction. Recent projects include <a target="_blank" href="http://1beat.org">1beat.org</a> and Echo. Shiran is currently pursuing his Masters in Computers Science at Boston University.</p>
								</div>
							</div>
						
						</div>
						<div class="slide contact six" name="contact">
							<div class="info one">
								<h3>Contact Us</h3>
								<p>If you have questions, please contact </p>
								<a href="mailto:echo@foundsoundnation.org">echo@foundsoundnation.org</a>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			');
	
  	//if(strlen($lgn) > 3) {
  		print('<div id="num">'.$_SESSION["lgn"].'</div>');
	//}
	print('</body></html>');
			
}
?>

<?php 

session_start();
if($_SESSION['lgn']) {
	luci();
} else if($o != "admin") {
	luci();
}
 if(isset($_POST['lgn'])) {
    if($_POST['pass'] == 'lunawool') {
        $_SESSION['lgn'] = 1;
    } else if($_POST['pass'] == 'solarflare') {
        $_SESSION['lgn'] = 2;
    }  else if($_POST['pass'] == 'sunburst') {
        $_SESSION['lgn'] = 3;
    }  else if($_POST['pass'] == 'roseleaf') {
        $_SESSION['lgn'] = 4;
    }  else if($_POST['pass'] == 'stonevine') {
        $_SESSION['lgn'] = 5;
    }  else if($_POST['pass'] == 'treeroot') {
        $_SESSION['lgn'] = 6;
    }  else if($_POST['pass'] == 'starship') {
        $_SESSION['lgn'] = 7;
    }  else if($_POST['pass'] == 'blueribbon') {
        $_SESSION['lgn'] = 8;
    }  else if($_POST['pass'] == 'jazzelectra') {
        $_SESSION['lgn'] = 9;
    }   else if($_POST['pass'] == 'soundfound') {
        $_SESSION['lgn'] = 0;
    }   else if($_POST['pass'] == 'echodrops') {
        $_SESSION['lgn'] = "admin";
    }


}
?>

