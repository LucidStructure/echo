$( '#tac' ).click(function() {
	$('.slide.two').on('click', '.autre', function() {
		console.log("changed")	
		var c = $(this).attr('class').split(' ')[1];

		$('.autre.a').removeClass('a')
		$(this).addClass('a')


		$('.exit').click()
		$('.box').css('display','none')
		$('.box ').removeClass('featured')
	    

		$('.box.'+c).fadeIn(500)	
	    $('.x.'+c).addClass('featured')
	})

	var ens = ["One","Two","Three","Four","Five","Six","Seven","Eight","Nine"]

	var loc = ["tl","tr","bl","br"]
	var text = ''


	$.getJSON('j/json/ensembles.json',  function(e) {
		for(var i = 0; i<9; i++) {
			n = i+1
			pos = 0
			text += '<div class="box e'+n+'"><h1> Ensemble '+ens[i]+'</h1><div class="x e'+n+'"><div class="exit">back</div>'
			for(var x in e.data) {
				if(e.data[x].ensemble == ens[i]) {
					//console.log(i + " " + e.data[x].name)
					text += draw(e.data[x],loc[pos])
					pos++
				}
			}
			text += '</div></div>'
		}
		text += '<div class="autres cf"> \
							<div class="autre e1 a"><p>1</p></div> \
							<div class="autre e2"><p>2</p></div> \
							<div class="autre e3"><p>3</p></div> \
							<div class="autre e4"><p>4</p></div> \
							<div class="autre e5"><p>5</p></div> \
							<div class="autre e6"><p>6</p></div> \
							<div class="autre e7"><p>7</p></div> \
							<div class="autre e8"><p>8</p></div> \
							<div class="autre e9"><p>9</p></div> \
						</div>'
		document.getElementById("ens").innerHTML += text
		$('.box').css('display','none')
		var rand = Math.floor(Math.random() * 9) + 1
		console.log(rand)
		$('.box.e'+rand).fadeIn(500)
	    $('.x.e'+rand).addClass('featured')
	    $('.autre.a').removeClass('a')
	    $('.autre.e'+rand).addClass('a')

	})


});

function draw(obj,num) { //DRAW BOX
	var t = ''
	if(obj.type == "aud") {
		t = audio(obj,num)
	} else if(obj.type == "vid") {
		t = video(obj,num)
	} else if(obj.type == "img") {
		t = img(obj,num)
	} else if(obj.type == "imgo") {
		t = imgo(obj,num)
	} else if(obj.type == "txt") {
		t = txt(obj,num)
	}
	return t
}

function audio(obj,num) {
	var t = '<div class="'+num+' feat aud"> \
				<h2>'+obj.name+'</h2> \
				<h3>'+obj.title+'</h3>'
				if(obj.embed != undefined) {
					t += '<div class="s aud">'+obj.embed+'</div>'
				}
				
			t+ '</div>'
	return t
}

function video(obj,num) {
	var t = '<div class="'+num+' feat vid"> \
			<h2>'+obj.name+'</h2> \
			<h3>'+obj.title+'</h3>'
			if(obj.embed != undefined) {
				t += '<div class="s vid">'+obj.embed+'</div>'
			}
		t += '</div>'
	return t
}

function txt(obj,num) {
	var t= '<div class="'+num+' feat txt"> \
				<h2>'+obj.name+'</h2> \
					<h3>'+obj.title+'</h3> \
					<div class="s txt"> \
						<div class="desc"> \
							<h2>Title of Paper:  '+obj.title+'</h2> \
							<a href="'+obj.sharing+'"><img width="100%" src="'+obj.pic+'"></a> \
						</div> \
						<div class="dwn"><p>download</p></div> \
					</div> \
			</div>'
	return t
}

$(window).load(function() {
	rX2()
	
});

$(document).ready(function() {
	pret2()
	
});
function pret2() { 
	set2()
	clicks2() 
}

function set2() {
	var sA = $('.mct .slides .slide.a')
	sA.nextAll().addClass('r')
	sA.prevAll().addClass('l')
	rX2()

	
	var sa = $('.featured .feat .s.img .ss .sli.a')
	sa.nextAll().addClass('r')
	sa.prevAll().addClass('l')
	$('.featured .feat .s.img .arw.r').addClass('o')


}

function rX2() {
	console.log("hold up")
	var sW = $('.mct .slides')
	var sA = $('.mct .slides .slide.a')
	var sH = sA.height()
	sW.css('height', sH)
}

function clicks2() {
	var nO = $('.mct .nav li')

	nO.click(function() {
		var hash = $(this).attr('title')
		var nN = $(this).attr('name')
		var nS = $('.mct .slides .slide.'+ nN +'')
		var sA = $('.mct .slides .slide.a')
		var nA = $('.mct .nav li.a,.mct h1.a')

		if(!$(this).hasClass('a')) {
			nS.addClass('n')
			$(this).addClass('a')
			setTimeout(function() {	
				nS.addClass('a')
				if(nS.hasClass('r')) {
					sA.addClass('l')
					nS.removeClass('r')
				} else if(nS.hasClass('l')){
					nS.removeClass('l')
					sA.addClass('r')
				}
				setTimeout(function() {
					sA.removeClass('a')
					nS.prevAll().removeClass('r').addClass('l')
					nS.nextAll().removeClass('l').addClass('r')
					nS.removeClass('n')
					nS.next().removeClass('n')
					nS.prev().removeClass('n')
					rX()
					nA.removeClass('a')
				}, 150);	
			}, 100);
		}
	})
	$('.upload .upload').click(function(){
		$('.ovly').fadeIn(400)
	})
	$('.ovly').click(function() {
		$(this).fadeOut(400)
	})
	$('ul.nav li').click(function(){
		var a = $('ul.nav li.a')

		a.removeClass('a')
		$(this).addClass('a')
	})
	$('.filter .o').click(function(){
		var name = $(this).attr('name')
		console.log(name)
		if($('.strip .s').hasClass(name)) {
			$('.strip .s').fadeOut(400)
			setTimeout(function(){
				$('.strip .s.'+name).fadeIn(400)
			}, 400)
			
		}
	})
	$('#abt').on('click', '.featured .feat', function() {
		var exit = $('.featured .exit')
		var n = $(this).nextAll()
		var p = $(this).prevAll()
		var s = $('.featured .feat .s')

		$(this).addClass('x')
		$(this).children('s').fadeIn(300)
		n.addClass('o')
		p.addClass('o')
		setTimeout(function(){
			$(this).addClass('m')
			n.addClass('z')
			p.addClass('z')
		},300)
		setTimeout(function(){
			exit.fadeIn(1000)
			s.addClass('x')
		},1000)
	})
	$('#abt').on('click', '.featured .exit', function() {
		$(this).css('display', 'none')
		var x = $('.featured .feat.x')
		var n = x.nextAll()
		var p = x.prevAll()
		var s = $('.featured .feat .s')

		x.removeClass('x')
		s.removeClass('x')
		setTimeout(function(){
			x.removeClass('m')
		},300)
		setTimeout(function(){
			n.removeClass('o')
			p.removeClass('o')
		},110)
		setTimeout(function(){
			n.removeClass('z')
			p.removeClass('z')
		},100)
	})


	$('.featured .feat .s.img .arws .arw.l').click(function() {
		var sa = $('.featured .feat .s.img .ss .sli.a')
		var r = $('.featured .feat .s.img .arws .arw.r')
		var n = sa.next()

		r.removeClass('o')
		sa.removeClass('a').addClass('l')
		n.removeClass('r').addClass('a')

		if(n.hasClass('ls')) {
			$('.featured .feat .s.img .arws .arw.l').addClass('o')
		}
	})
	$('.featured .feat .s.img .arws .arw.r').click(function() {
		var sa = $('.featured .feat .s.img .ss .sli.a')
		var l = $('.featured .feat .s.img .arws .arw.l')
		var p = sa.prev()

		l.removeClass('o')
		sa.removeClass('a').addClass('r')
		p.removeClass('l').addClass('a')

		if(p.hasClass('f')) {
			$('.featured .feat .s.img .arws .arw.r').addClass('o')
		}
	})
}




/*
function img(obj) {
	var pics = obj.pic.split(",")
	var t = '<div class="bl feat img"> \
			<h2>'+obj.name+'</h2> \
			<h3>'+obj.title+'</h3> \
			<div class="s img"> \
				<div class="ss"> \
					<div class="sli f a">
						<img width="100%" src="http://www.hdbackgroundpoint.com/wp-content/uploads/2014/03/25/8775521-cute-kitten-wallpaper-free.jpg" />
					</div>
					<div class="sli">
						<img width="100%" src="http://www.hdbackgroundpoint.com/wp-content/uploads/2014/03/25/r6t5.jpeg" />
					</div>
					<div class="sli">
						<img width="100%" src="http://exocticpetplus.com/wp-content/uploads/2014/03/Bengal-Kittens.jpg" />
					</div>
					<div class="sli ls">
						<img width="100%" src="http://www.hdbackgroundpoint.com/wp-content/uploads/2014/03/25/r6t5.jpeg" />
					</div>
				</div>
				<div class="arws">
					<div class="arw l"></div>
					<div class="arw r"></div>
				</div>
				<div class="desc">
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
			</div>
		</div>'
}*/

/*
function imgo(obj) {
	var t = '<div class="bl fear img"> \
				<h2>'+obj.name+'</h2> \
					<h3>'+obj.title+'</h3> \
						<div class="s imgo"> \
							<div class="sli"> \
								<a href="'+obj.sharing+'"><img width="100%" src="'+obj.pic+'"></a> \
							</div>'
	if(obj.excerpt != undefined) {
		t += '<div class="desc"><p>'+obj.excerpt+'</p></div>'
	}
	t+= '</div>'
	document.getElementById("ens").innerHTML += t
							
}*/

	
