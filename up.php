<?php 

    $file = 'j/json/chart.json';

    if (file_exists($file)) {
        $con = file_get_contents($file);
        $json = json_decode($con);
        $entry = array(array(
            "i"=>$_POST['id'],
            "n"=>$_POST['name'],
            "t"=>$_POST['title'],
            "f"=>$_POST['file'],
            "m"=>$_POST['mode'],
            "r"=>$_POST['ring'],
            "p"=>$_POST['parent'],
            "s"=>$_POST['self']
        ));

        if($_POST["edit"] == "editing") {
            $found = false;
            foreach($json->gxy->$_POST['sys'] as $item) {
                if($item->i == $_POST["id"]) {
                    $found = true;
                    $item->i = $_POST['id'];
                    $item->n = $_POST['name'];
                    $item->t = $_POST['title'];
                    $item->f = $_POST['file'];
                    $item->m = $_POST['mode'];
                    $item->r = $_POST['ring'];
                    $item->p = $_POST['parent'];
                    $item->s = $_POST['self'];
                }
            }
        } else if(isset($_POST["del"])) {
            $stars = explode(",", $_POST['grp']);
            foreach($stars as $star) {
                $i = 0;
                foreach($json->gxy->$_POST['sys'] as $item) {
                    if($item->i == $star) {
                       array_splice($json->gxy->$_POST['sys'],$i,1);                 
                    }
                    $i++;
                }    
            }
        } else {
            $json->gxy->$_POST['sys'] = array_merge($json->gxy->$_POST['sys'], $entry);
        }
        
        
        $j = json_encode($json,JSON_PRETTY_PRINT);
        file_put_contents($file, $j);
    }

    // function delkids($r,$p) {
    //      foreach($json->gxy->$_POST['sys'] as $item) {
    //         if($item->r == $r && $item->p == $p) {
    //             array_splice($json->gxy->$_POST['sys'],$i,1);

    //         }
    //      }
    // }


?>
