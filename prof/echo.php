<?php
function luci() {
print('
<!DOCTYPE html>
<html>
<head>
	<link type="text/css" rel="stylesheet" href="ui/echo/ui/mode.css" />
	<link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Signika:400,600" rel="stylesheet" type="text/css">

	<title>Echo</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="ui/echo/ui/j/luci.js"></script>
</head>
<body>
	<div class="wrp">
		<div class="mct">
			<a href="http://1beat.org" class="hd-logo">
				<img src="https://s3-us-west-2.amazonaws.com/slideshow13/logo.png" />
			</a>
			<h1>OneBeat</h1>
			<h2>((echo))</h2>
			<ul class="nav">
				<li name="one">About</li> |
				<li class="a" name="two">Fellows</li> |
				<li name="three">Dates</li> |
				<li name="four">Contact</li> |
				<li name="five">Support</li> 
			</ul>
			<div class="slides">
				<div class="slide one">
					<div id="about" class="info one">
						<h3>About OneBeat Echo</h3>
						<p><strong>OneBeat Echo</strong> is an online conference that amplifies the mission of <a href="http://1beat.org">OneBeat</a> through global, web-based, artistic exchange and dialogue. OneBeat Echo connects innovative musicians, artists, writers, and thinkers as Echo Fellows in a month-long interactive exchange, during which time Fellows engage in an online laboratory of artistic and scholarly ideas and experimentation. This interactive exchange culminates in a 30-day online exhibition that showcases these new works, and encourages exchange with the wider public. Echo proceeds in three stages:</p>	
						<p><strong>I. Sharing: In the first stage of OneBeat Echo (September 1, 2014)</strong>, Fellows are asked to offer existing creative or scholarly work that they would like to share with the Echo community, reflecting the mission of <a href="http://1beat.org">OneBeat</a>: "to celebrate the transformative power of the arts through the creation of original, inventive music; to develop ways that music can make a positive impact on our local and global communities; and to foster mutual understanding and cooperation across geographic and cultural barriers."</p>
						<p><strong>II. Collaborating: In the second stage of OneBeat Echo (September 15-October 15, 2014)</strong>, Echo Fellows are connected to a small group of other Echo Fellows in "Ensembles," and asked to collaborate and/or respond to one another\'s work through creative or scholarly work. Collaborative online tools such as: <a href="http://www.kompoz.com/">Kompoz</a>, <a href="http://google.com/drive">Google Drive</a>, <a href="http://facebook.com/">Facebook</a>, among others, are utilized individually or collectively to keep Echo Fellows connected and collaborating.</p>
						<p><strong>III. Exhibiting: In the third and last stage of OneBeat Echo (November 15th-December 15th, 2014)</strong>, the collective creative and scholarly works of all Echo Fellows are posted online on the Echo website for the global public to view, share, and interact with the various work of the Echo Fellows.</p>
					</div>
					<div class="info two">
						<h3>About OneBeat</h3>
						<p><a href="http://1beat.org">OneBeat</a> is an international cultural exchange that celebrates the transformative power of the arts through the creation of original, inventive music, and people-to-people diplomacy. OneBeat brings young musicians (ages 19-35) from around the world to the U.S. for one month to collaboratively write, produce, and perform original music, and develop ways that music can make a positive impact on our local and global communities. More than a performance program, OneBeat balances three principles -- dialogue, creation, and social engagement -- to foster mutual understanding and cooperation among citizens of the world.</p>
					</div>
				</div>
				<div class="slide a two">
					<div class="info three">
						<h3>Echo Fellows</h3>
						<div class="ensemble one">
							<p><strong>Ensemble 1</strong>
							<p>Natasha Humera Ejaz -  Producer, Vocalist from Pakistan | <a href="mailto:natashahejaz@gmail.com">Email</a> | <a href="http://natashahejaz.wix.com/dotsandlines">Sharing</a></p>
							<p>Roshy Kheshti - Academic from USA  | <a href="mailto:rkheshti@ucsd.edu">Email</a> | <a href="http://ethnoacoustigraphy.blogspot.com/">Sharing</a></p>
							<p>Svetlana Maras - Composer from Serbia  | <a href="mailto:svetlanamarash@gmail.com">Email</a> | <a href="http://www.armoniapolis.com">Sharing</a></p>
							<p>Steven Baker - Academic from USA  | <a href="mailto:stefanofornaio@gmail.com">Email</a> | <a href="https://www.youtube.com/watch?v=vlVNbfnWRDc&list=UUjtWAPMiQVtXGERUv5tyy2Q">Sharing</a></p>
						</div>
						<div class="ensemble two">
							<p><strong>Ensemble 2</strong>
							<p>PPS - MC from Senegal | <a href="mailto:paulpissety23@gmail.com">Email</a> | <a href="https://www.youtube.com/watch?v=3MJ3ONbVWKI">Sharing</a></p>
							<p>Sasha Grbich - Academic from Australia | <a href="mailto:sasha.grbich@gmail.com">Email</a> | <a href="http://sashagrbich.com/works/handmade-diasater-deluge/">Sharing</a></p>
							<p>Fuerza Mexica - Emcee from USA | <a href="mailto:fuerzamexica@gmail.com">Email</a> | <a href="http://soundcloud.com/fuerzamexica/situations/s-iqyW9">Sharing</a> <a href="https://soundcloud.com/fuerzamexica/atterizo-fuerzamexica/s-GqzCZ">+</a></p>
							<p>Dmitri Burmistrov - Beatboxer, Producer from Russia  | <a href="mailto:sremvushi@gmail.com">Email</a> | <a href="https://www.youtube.com/watch?v=Jn5xYiUoKMQ&feature=youtu.be">Sharing</a></p>
						</div>
						<div class="ensemble three">
							<p><strong>Ensemble 3</strong>
							<p>Malabika Brahma  - Vocalist from India  | <a href="mailto:brahmamalabika@gmail.com">Email</a> | <a href="http://vimeo.com/104689092">Sharing</a></p>
							<p>Giorgos Stylianou - Lute player, Sound Artist from Cyprus | <a href="mailto:stylianou.george@gmail.com">Email</a> | <a href="https://www.youtube.com/watch?v=-xveI0IgVHk&feature=youtu.be">Sharing</a></p>
							<p>Shannon Werle  -  Academic from USA | <a href="mailto:shannonwerle@gmail.com">Email</a> | <a href="http://www.shannonwerle.com/tokyo/">Sharing</a></p>
						</div>
						<div class="ensemble four">
							<p><strong>Ensemble 4</strong>
							<p>Lama Zakharia &amp; Jana Zeinedinne  -  Vocalists from Jordan | <a href="mailto:lamazakharia90@gmail.com">Email</a> | <a href="mailto:janaz22@yahoo.com">Email</a> | <a href="http://www.youtube.com/watch?v=toCGtnUz7DI&feature=youtu.be">Sharing</a></p>
							<p>Helio Vanimal - Singer, Instrumentalist from Mozambique  | <a href="mailto:heliod2000@yahoo.com.br">Email</a> | <a href="http://youtu.be/pgC_a3Uat58?list=UUcoIcmJA6mbqQp4g5CFo-wQ">Sharing</a></p>
							<p>Jay Hammond - Banjo Player, Academic from USA | <a href="mailto:jere.w.hammond@gmail.com">Email</a> | <a href="https://soundcloud.com/hammond_jay/sets/album-drafts">Sharing</a></p>
							<p>Helen Morgan - Artist, Sounder from United Kingdom  | <a href="mailto:helen.morgan99@gmail.com">Email</a> | <a href="https://www.flickr.com/photos/helen-morgan/sets/72157646867598737/">Sharing</a></p>
						</div>
						<div class="ensemble five">
							<p><strong>Ensemble 5</strong>
							<p>Wei Wei - Composer from China  | <a href="mailto:vavabond@gmail.com">Email</a> | <a href="https://soundcloud.com/vavabond/distance">Sharing</a></p>
							<p>Elen Fl&uuml;gge - Writer, Sound Artist from Germany/USA | <a href="mailto:elen.flug@gmail.com">Email</a> | <a href="https://s3-us-west-2.amazonaws.com/onebeatecho/Elen_Flugge.pdf">Sharing</a></p>
							<p>Weronika Partyka -  Singer, Clarinet Player from Poland | <a href="mailto:weronikapartyka@gmail.com">Email</a> | <a href="https://soundcloud.com/soundverka/oj-kozo-kozo">Sharing</a></p>
							<p>Aditi Bhagwat -  Percussionist from India | <a href="mailto:aditteebhagwat@gmail.com">Email</a> | <a href="https://soundcloud.com/found-sound-nation/aditi-bhagwat">Sharing</a></p>
						</div>
						<div class="ensemble six">
							<p><strong>Ensemble 6</strong>
							<p>Aimilia Karapostoli - Academic from Greece | <a href="mailto:aditteebhagwat@gmail.com">Email</a> | <a href="https://s3-us-west-2.amazonaws.com/onebeatecho/Aimilia_Karapostoli.pdf">Sharing</a></p>
							<p>Ruth Danon - Vocalist, Producer from Israel  | <a href="mailto:ruthdanon@yahoo.com">Email</a> | <a href="https://soundcloud.com/ruthdanon/little-piano-tune">Sharing</a> <a href="https://www.youtube.com/watch?v=6KGsFgDMuNc">+</a></p>
							<p>Leah Barclay - Composer, Sound Artist from Australia | <a href="mailto:leahbarclay@me.com">Email</a> | <a href="https://s3-us-west-2.amazonaws.com/onebeatecho/Leah_Barclay.pdf">Sharing</a></p>
							<p>Sarah Alden - Violinist, Singer from USA  | <a href="mailto:snappysarah@gmail.com">Email</a> | <a href="https://soundcloud.com/sarah-alden/live-barbes-cypriots-sunrises">Sharing</a></p>
						</div>
						<div class="ensemble seven">
							<p><strong>Ensemble 7</strong>
							<p>Nicole Hodges Persley - Academic, Theater Artist from USA | <a href="mailto:hodgespersley@ku.edu">Email</a> | <a href="https://soundcloud.com/found-sound-nation/nicole-hodges-persley">Sharing</a> <a href="https://s3-us-west-2.amazonaws.com/onebeatecho/Nicole_Hodges_Persley.pdf">+</a></p>
							<p>Ahmed Mabrok - MC from Egypt  | <a href="mailto:underockz2@hotmail.com">Email</a> | <a href="https://soundcloud.com/revrecordz/eza-el-shams-gher2et">Sharing</a></p>
							<p>Ruben Coll -  Researcher, Sound Activist from Spain | <a href="mailto:rubencollhernandez@gmail.com">Email</a> | <a href="https://s3-us-west-2.amazonaws.com/onebeatecho/Ruben_Coll.pdf">Sharing</a></p>
							<p>Wey Chong Chua - Ruan from Malaysia   | <a href="mailto:neilchua810@gmail.com">Email</a> | <a href="https://soundcloud.com/neilchua/dawnofthemist">Sharing</a></p>
						</div>
						<div class="ensemble eight">
							<p><strong>Ensemble 8</strong>
							<p>Elyse Tabet - Producer, Sound Artist from Lebanon   | <a href="mailto:elysetabet@yahoo.com">Email</a> | <a href="https://soundcloud.com/litterairlines/light-you-cast">Sharing</a></p>
							<p>Grupal Crew Collective - Artists from Columbia, Italy, and Spain  | <a href="mailto:recmade@gmail.com">Email</a> | <a href="https://proyectodemos.bandcamp.com/releases">Sharing</a> <a href="http://proyectodemos.wordpress.com/acerca-de/">+</a></p>
							<p>Sharrell Luckett - Academic, Performance Artist from USA  | <a href="mailto:sluckett@csudh.edu">Email</a> | <a href="https://soundcloud.com/lovebubble-1/skinny-me">Sharing</a></p>
							<p>Amine Metani - Electronic Music Producer from Tunisia  | <a href="mailto:acideamine@free.fr">Email</a> | <a href="https://soundcloud.com/mettani/prarthana">Sharing</a></p>
						</div>
						<div class="ensemble nine">
							<p><strong>Ensemble 9</strong>
							<p>Darbuka Siva -  Drummer, Bassist from India | <a href="mailto:shiva82@gmail.com">Email</a> | <a href="https://soundcloud.com/darbukasiva/bean">Sharing</a></p>
							<p>Laura Plana Gracia - Academic from United Kingdom | <a href="mailto:lauraplanagracia@gmail.com">Email</a> | <a href="https://soundcloud.com/elektronische-music/lara-pearl-median-ages-oscillators3raw">Sharing</a></p>
							<p>Jeb Middlebrook - Sound Archivist, Researcher from USA | <a href="mailto:jmiddlebrook@csudh.edu">Email</a> | <a href="https://soundcloud.com/jebmiddlebrook/hip-hop-surveillance-ft-dead">Sharing</a></p>
						</div>				
					</div>		
				</div>
				<div class="slide three">
					<div class="info five">
						<h3>Important Dates</h3>
						<p><span>September 1:</span> Echo Fellows Submissions Due (11:59PM GMT)</p>
						<p><span>October 15:</span> Echo Ensembles Content Due (11:59PM GMT)</p>
						<p><span>November 15 - December 15: </span> Public Exhibition</p>
					</div>
				</div>
				<div class="slide four">
					<div class="info six">
						<h3>Contact Us</h3>
						<p>If you have questions, please contact </p>
						<a href="mailto:echo@foundsoundnation.org">echo@foundsoundnation.org</a>
					</div>
				</div>
				<div class="slide five">
					<div class="info seven">
						<h3>Support</h3>
						<div class="p">
							<p><a href="http://jebmiddlebrook.com">Jeb MiddleBrook</a>, is Co-Founder and Curator of OneBeat Echo. He is Assistant Professor of Sociology at California State University, Dominguez Hills in Los Angeles, where he researches and teaches music, justice, and ethnic studies. He holds a Ph.D. and M.A. in American Studies and Ethnicity from the University of Southern California, and a B.A. in Ethnic Studies from the University of Minnesota. His current work explores the sound of prison in popular culture, policy, and protest, and can be viewed at: <a href="http://prisonmusic.net">PrisonMusic.net</a>. </p>
						</div>
						<div class="p">
							<p><a href="">Chris Marianetti </a> , Co-Founder &amp; Artistic Director of Found Sound Nation, is a composer and producer from Albuquerque, New Mexico. His works have premiered at the International Gaudeamus Competition, Merkin Concert Hall, Massachusetts Museum of Modern Art, The Stone, and the Tenri Cultural Institute among others. Recent works include: an animated score for brass band projected onto the sides of Manhattan buildings, and a symphonic work for a group of lowrider automobiles for the 2012 ISEA festival in New Mexico. With Found Sound Nation Chris has led site-specific music projects in New York City, India, Mexico, Zimbabwe, Italy, and Switzerland and was recently awarded the PopTech Social Innovation Fellows Award. Chris is an active teaching artist with the Weill Music Institute at Carnegie Hall. Chris trained in composition at the Accademia Internazionale della Musica in Milan, Italy and received his Master\'s in music composition from Brooklyn College Conservatory of Music.</p>
						</div>
						<div class="p">
							<p><a href="">Elena Moon Park </a> is a musician and educator living in Brooklyn, NY. Originally from Oak Ridge, TN, she studied anthropology and ethnomusicology at Northwestern University and completed her M.A. in Urban Policy from The New School in New York. Before moving to the city, Elena served as a development consultant for several grassroots social justice organizations on the south and west sides of Chicago. Today, she manages programs for Bang on a Can\'s Found Sound Nation and actively engages in outreach with youth arts groups across the United States. Elena has recently released Rabbit Days and Dumplings, an album for families featuring folk and children\'s music from East Asia. Elena still finds the time to play fiddle, trumpet, mandolin, and sing with Dan Zanes and Friends on stages worldwide. </p>
						</div>
						<div class="p">
							<p><a href="">Jeremy Thal</a>, Co-Founder & Artistic Director of Found Sound Nation, is a horn player, composer, and educator. He studied horn, ethnomusicology, and Chinese at Northwestern University, and continues to work as a performer, playing in major music festivals and obscure bookstores around the world. With fellow co-founder Chris Marianetti, Jeremy co-designed Found Sound Nation\'s approach to socially-engaged music creation, and has co-led FSN projects in Haiti, Zimbabwe, New Orleans, Mexico, Indonesia, Italy, and Switzerland. For the last 3 years, Jeremy has served as a lead teaching artist at the Weill Music Institute at Carnegie Hall, running innovative music production workshops with incarcerated youth in New York City. A founder of ZeroBit Music, he composes and produces music for film, theater, web sites, and video games. As a horn player Jeremy has recorded and toured with indie rock heavyweights Jeff Mangum and The National, and leads his own band, Briars of North America. </p>
						</div>
						<div class="p">
							<p><a href="">Ezra Tenenbaum</a> is a New York-based performer and music producer. Ezra joined Found Sound Nation as a Teaching Artist in 2010 and has contributed to projects at Blair Grocery in New Orleans, Bronx\'s Horizons Juvenile Center, Brooklyn Prospect Charter School, and Brooklyn Community Arts and Media High School. He has performed at Glasslands, Death By Audio, Dance New Amsterdam and and the Center for Performance Research. In 2011, his band Strange Shapes released Star Chart on Japanese label, Rallye. </p>
						</div>
						<div class="p">
							<p><a href="">Miles Bridges</a>  is a producer and MC currently living in Boston, MA where he is finishing a BA in French language and litterature. Originally from Oakland, California, Miles gained an interest in communication and education through his experiences competiting and teaching on the James Logan Forensics team. </p>
						</div>
					</div>
				</div>
				</div>
				<div class="info eight cf">
					<div class="sponsors">
						<div class="big cf">
							<a href="http://exchanges.state.gov/" class="logo gov">
								<h4>a program of</h4>
							</a>
							<a href="http://foundsoundnation.org/curations/" class="logo fsn">
								<h4>produced by</h4>
							</a>
						</div>
						<div class="med cf">
							<a href="http://bangonacan.org/" class="logo boc">
								<h4>administered by</h4>
							</a>
							<a href="http://www.rsclark.org/" class="logo rsc">
								<h4>additional major support provided by</h4>
							</a>
						</div>
						<p class="cp"><strong>OneBeat</strong> is an initiative of the U.S. Department of State\'s Bureau of Educational and Cultural Affairs, & produced by Found Sound Nation.</p>
						<div class="c mch apl">
							<div id="mc_embed_signup">
								<form action="http://1beat.us2.list-manage.com/subscribe/post?u=beac208290c837877b9f4826a&amp;id=aca92b394a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
									<div class="mc-field-group">
										<label for="mce-EMAIL">Sign Up for our Newsletter</label>
										<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
									</div>
									<div id="mce-responses" class="clear">
										<div class="response" id="mce-error-response" style="display:none"></div>
										<div class="response" id="mce-success-response" style="display:none"></div>
									</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								    <div style="position: absolute; left: -5000px;"><input type="text" name="b_beac208290c837877b9f4826a_aca92b394a" tabindex="-1" value=""></div>
								    <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
								</form>
							</div>
						</div>
						<div class="social cf">
							<a href="https://twitter.com/1beatmusic" class="c s tw"></a>
							<a href="https://www.facebook.com/1beatmusic" class="c s fb"></a>
							<a href="https://www.youtube.com/user/FoundSoundNation" class="c s yt"></a>
							<a href="http://instagram.com/1beatmusic" class="c s ig"></a>
						</div>
					</div>
				</div>
		</div>
	</div>
</body>
<!-- lucidstructure - miles@lucidstructure.co-->
</html>');
}
?>
<?php 
session_start();
if($_SESSION['lgn']) {
    luci();
} else if(isset($_POST['lgn'])) {
    if($_POST['pass'] == '1beatecho') {
        $_SESSION['lgn'] = true;
    }
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="ui/j/lgn.js"></script>
<link type="text/css" rel="stylesheet" href="ui/echo/ui/mode.css" />
<div class="lgn">
<input class="pw" type="password" placeholder="pass">
<div id="log">login</div>
</div>

