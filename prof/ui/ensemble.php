<div class="share">	
	<h1>Ensemble One</h1>	
	<div class="featured">
		<div class="exit">back</div>
		<div class="tl feat vid">
			<h2>Person Name</h2>
			<h3>Title</h3>
			<div class="s vid">
				<iframe height="400" src="//www.youtube.com/embed/YclppeD82lo" frameborder="0" allowfullscreen></iframe>
				<div class="desc">
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
			</div>
		</div>
		<div class="tr feat aud">
			<h2>Person Name</h2>
			<h3>Title</h3>
			<div class="s aud">
				<iframe width="100%" height="400" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/92537771&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
				<div class="desc">
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
			</div>
		</div>
		<div class="bl feat img">
			<h2>Person Name</h2>
			<h3>Title</h3>
			<div class="s img">
				<div class="ss">
					<div class="sli f a">
						<img width="100%" src="http://www.hdbackgroundpoint.com/wp-content/uploads/2014/03/25/8775521-cute-kitten-wallpaper-free.jpg" />
					</div>
					<div class="sli">
						<img width="100%" src="http://images4.fanpop.com/image/photos/16100000/Cute-Kitten-kittens-16123158-1280-800.jpg" />
					</div>
					<div class="sli">
						<img width="100%" src="http://exocticpetplus.com/wp-content/uploads/2014/03/Bengal-Kittens.jpg" />
					</div>
					<div class="sli ls">
						<img width="100%" src="http://www.hdbackgroundpoint.com/wp-content/uploads/2014/03/25/r6t5.jpeg" />
					</div>
				</div>
				<div class="arws">
					<div class="arw l"></div>
					<div class="arw r"></div>
				</div>
				<div class="desc">
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
			</div>
		</div>
		<div class="br feat txt">
			<h2>Person Name</h2>
			<h3>Title</h3>
			<div class="s txt">
				<div class="desc">
					<h2>Title of Paper:  it's so good (excerpt)</h2>
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
					in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
					sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
				<div class="dwn"><p>download</p></div>
			</div>
		</div>
	</div>
	<div class="autres cf">
		<div class="autre"><p>2</p></div>
		<div class="autre"><p>3</p></div>
		<div class="autre"><p>4</p></div>
		<div class="autre"><p>5</p></div>
		<div class="autre"><p>6</p></div>
		<div class="autre"><p>7</p></div>
		<div class="autre"><p>8</p></div>
		<div class="autre"><p>9</p></div>
	</div>
</div>

